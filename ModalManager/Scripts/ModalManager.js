﻿var ModalManager = function () {

    this.ShowSuccessMessage = function (Msg) {
        var ModalObj = BuildNotifyModal('info', 'succeeded', 'Section was added successfully');
        $(ModalObj).modal('show');
        setTimeout(function () {
            $(ModalObj).modal('hide');
        }, 2000)
    }


    var BuildNotifyModal = function (NotifyType,NotifyMessage) {
        // success:glyphicon glyphicon-ok-sign
        //info:glyphicon glyphicon-info-sign
        //warn:glyphicon glyphicon-warning-sign
        //error:glyphicon glyphicon-remove-sign
        var MsgType = null;
        var MsgTitle = null;
        var MsgLogo = null;
        if (NotifyType.toLowerCase() == 'info')
        {
            MsgType = 'info';
            MsgTitle = 'Succeede';
            MsgLogo = 'glyphicon-ok-sign';
        }
        else if (NotifyType.toLowerCase() == 'success')
        {

        }
        else if (NotifyType.toLowerCase() == 'error') {

        }
        var Modal = $('<div class="modal fade" tabindex="-1" role="dialog">' +
  '<div class="modal-dialog" role="document">' +
    '<div class="modal-content info">' +
      '<div class="modal-body">' +
        '<h3 class="info"><span class="glyphicon glyphicon-ok-sign"></span><strong>succeeded</strong></h3>' +
        '<h4>Section Peaur was added successfully</h4>' +
      '</div>' +
    '</div>' +
  '</div>' +
'</div>');
        return Modal;
    }
}